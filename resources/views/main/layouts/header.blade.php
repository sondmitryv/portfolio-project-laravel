<header id="header">
    <div class="container main-menu">
    	<div class="row align-items-center justify-content-between d-flex">
	    <div id="logo">
            <router-link to="/" exact><img src="/img/logo.png" alt="" title="" /></router-link>
	    </div>
            <nav id="nav-menu-container">
                <ul class="nav-menu">
                    <li>
                        <router-link to="/" exact>{{ __('nav.menu01')}}</router-link>
                    </li>
                    <li>
                        <router-link to="/about">{{ __('nav.menu02')}}</router-link>
                    </li>
                    @auth
                        <li>
                            <router-link to="/my-portfolio">{{ __('nav.menu04')}}</router-link>
                        </li>
                    @endauth
                    <li>
                        <router-link to="/all-works">{{ __('nav.menu041')}}</router-link>
                    </li>
                    @guest
                        <li><a href="/nova/login">{{ __('nav.menu05')}}</a></li>
                        <li><a href="/register">{{ __('nav.menu051')}}</a></li>
                    @endguest
                    @auth
                        <li><a href="/nova/dashboards/main">{{ __('nav.menu06')}}</a></li>
                    @endauth

                </ul>
            </nav><!-- #nav-menu-container -->
    	</div>
    </div>
</header><!-- #header -->
<nav id="mobile-nav">
    <ul class="" id="">
        <li>
			<router-link to="/" exact>{{ __('nav.menu01')}}</router-link>
		</li>
        <li>
            <router-link to="/about">{{ __('nav.menu02')}}</router-link>
        </li> 
        @auth
            <li>
                <router-link to="/my-portfolio">{{ __('nav.menu04')}}</router-link>
            </li>
        @endauth
        <li>
            <router-link to="/all-works">{{ __('nav.menu041')}}</router-link>
        </li>
        @guest
            <li><a href="/nova/login">{{ __('nav.menu05')}}</a></li>
            <li><a href="/register">{{ __('nav.menu051')}}</a></li>
        @endguest
        @auth
            <li><a href="/nova/dashboards/main">{{ __('nav.menu06')}}</a></li>
        @endauth
    </ul>
</nav>