import Home from './components/Home';
import About from './components/About';
import MyPortfolio from './components/MyPortfolio';
import AllPortfolio from './components/AllPortfolio';

export default{
    mode:'history',
    routes:[
        {
            path:'/',
            component: Home
        },
        {
            path:'/about',
            component: About
        },
        {
            path: '/my-portfolio',
            component: MyPortfolio
        },
        {
            path: '/all-works',
            component: AllPortfolio
        },

        
    ]

}
