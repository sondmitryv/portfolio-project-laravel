<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Nova\Fields\HasMany;
use Illuminate\Support\Facades\Auth;


class Profile extends Model
{
    public function tool(){
        return  $this->hasMany(Tool::class);
    }
    public function getProfile(){
        return $this->all();
    }
    
  
}
