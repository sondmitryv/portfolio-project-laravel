<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Profile;
use App\Work;
use App\User;
use Illuminate\Support\Facades\Auth;



class PortfolioController extends Controller
{
    public function myportfolio()
    {
        return $works = (app(Work::class))->getWorks();
    }
    public function allportfolio()
    {
       return (app(Work::class))->getAllWorks();
    }
}
