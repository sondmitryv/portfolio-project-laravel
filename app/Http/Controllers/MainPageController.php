<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;
use App\Tool;



class MainPageController extends Controller
{
    public function main()
    {
        return view('main.layouts.index');
    }

    public function index()
    {
       return  $profile = (app(Profile::class))->getProfile();
    }
   
    public function about()
    {
        $profile = (app(Profile::class))->getProfile();
        $tools = (app(Tool::class))->getAbout();
        return [ $profile, $tools ];
    }

    
}
