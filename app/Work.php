<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Work extends Model
{
    public function  user(){
        return $this->belongsTo(User::class);
    }
    public static function boot()
        {
            parent::boot();

            static::creating(function ($work) {
                $work->profile_id = auth()->user()->id;
            });
        }
        public function getWorks()
        {
            return $this->all()->where('profile_id', '1');  

        }
        public function getAllWorks()
        {
            return $this->all();
        }

}
