<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tool extends Model
{
    public function  profile(){
        return $this->belongsTo(Profile::class);
    }
    public function getAbout()
    {
       return  $this->all();
    }
   
}
